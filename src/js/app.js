"use strict"


const optionsJSON = {
    "position": "right",
    "mob_invert": false,
    "color": "#0acbd7"
}

function callbackWidget(data) {
    const container = document.querySelector('[data-widget-call]')
    const colorPrimary = data.color
    const mobInvert = data.mob_invert
    const buttonPosition = data.position

    if (colorPrimary) {
        container.style.setProperty('--color-primary', colorPrimary);
    }
    if(mobInvert) {
        document.querySelector('[data-callback-sticky]').classList.add('-lt-inverse-')
    }
    if (buttonPosition === 'left') {
        document.querySelector('[data-callback-wrapper]').classList.add('-inverse-')
    }
}


window.addEventListener("load", function (e) {

    callbackWidget(optionsJSON)

    document.addEventListener('click', (event) => {
        if (event.target.closest('[data-callback-open]')) {
            event.target.closest('[data-callback]').classList.add('lt-xbutton-active')
        }
        if (event.target.closest('[data-callback-close]')) {
            event.target.closest('[data-callback]').classList.remove('lt-xbutton-active')
            event.target.closest('[data-callback]').classList.remove('lt-status-busy')
        }
        if (event.target.closest('[data-callback-show]')) {
            document.querySelector('body').classList.add('-lt-callback-open-')
        }
        if (event.target.closest('[data-callback-hide]')) {
            document.querySelector('body').classList.remove('-lt-callback-open-')
        }
        if (event.target.closest('[data-success-close]')) {
            document.querySelector('body').classList.remove('-lt-success-open-')
        }
        if (event.target.closest('[data-widget-send]')) {
            console.log('send')
            event.target.closest('[data-callback]').classList.add('lt-status-busy')
        }
    })

    document.addEventListener('click', (event) => {
        if (event.target.closest('[data-callback-send]')) {
            document.querySelector('body').classList.remove('-lt-callback-open-')
            document.querySelector('body').classList.add('-lt-success-open-')
        }
    })
});


